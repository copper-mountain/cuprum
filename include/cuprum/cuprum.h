/*! \file

    Cuprum library API

 */


#include <cuprum/cudef.h>

#include <stdint.h>     // intXX_t support


#ifdef CU_PRAGMA_ONCE_SUPPORT
# pragma once
#endif


#ifndef CUPRUM_HEADER_B55F7A19E7BB4B838EA1F5D240773AAA
#define CUPRUM_HEADER_B55F7A19E7BB4B838EA1F5D240773AAA


CU_EXTERN_BEGIN_DECL


/* Cuprum library types, functions, etc */


CU_EXTERN_END_DECL


#endif /* CUPRUM_HEADER_B55F7A19E7BB4B838EA1F5D240773AAA */
