/*! \file

    Cuprum library preprocessing component

 */

#ifndef CUDEF_HEADER_F93942F89D394708B216CCC1A5918577
#define CUDEF_HEADER_F93942F89D394708B216CCC1A5918577


/* process build configuration */
#include <cuprum/predef/build_target.h>

/* process compiler information */
#include <cuprum/predef/compiler.h>

/* process target operation system */
#include <cuprum/predef/os.h>


/* additional macros */

/* language features support */
#if CU_COMPILER_STANDARD_IS_CXX17_UP
# define CU_SOURCE_LOCATION_SUPPORT 1
#endif /* C++20 */

/* __COUNTER__ support */
#if (CU_COMPILER_IS_GCC || CU_COMPILER_IS_CLANG || CU_COMPILER_IS_MSVC) /* __COUNTER__ */
# define CU_COUNTER_SUPPORT 1 
#endif

/* compile-time concatenation */
#define CU_MAGIC_CONCAT_(a,b) a##b
#define CU_MAGIC_CONCAT(a,b) CU_MAGIC_CONCAT_(a,b)

/* compile-time serialization */
#define CU_TO_STR_(s) #s
#define CU_TO_STR(s) CU_TO_STR_(s)

/* process supported features */
#if defined(CU_SOURCE_LOCATION_SUPPORT)
# include <source_location>
#else
# include <string.h>
#endif /* CU_SOURCE_LOCATION_SUPPORT */

/* current file name */
#if defined(CU_SOURCE_LOCATION_SUPPORT)
# define CU_FILE_NAME (std::source_location::current().file_name())
#else
# define CU_FILE_NAME (strrchr(__FILE__, '/') ? strrchr(__FILE__, '/') + 1 : __FILE__)
#endif /* CU_SOURCE_LOCATION_SUPPORT */

/* current line number */
#if defined(CU_SOURCE_LOCATION_SUPPORT)
# define CU_LINE_NUMBER (std::source_location::current().line())
#else
# define CU_LINE_NUMBER __LINE__
#endif /* CU_SOURCE_LOCATION_SUPPORT */

/* current function name */
#if defined(CU_SOURCE_LOCATION_SUPPORT) /* C++20 variant */
# define CU_FUNCTION_NAME (std::source_location::current().function_name())
#elif (CU_COMPILER_IS_GCC || CU_COMPILER_IS_CLANG) /* GCC and Clang */
# define CU_FUNCTION_NAME   __PRETTY_FUNCTION__
#elif defined(__FUNCSIG__) /* MSVC */
# define CU_FUNCTION_NAME __FUNCSIG__
#elif (CU_COMPILER_STANDARD_IS_CXX11_UP) /* C++11 */
# define CU_FUNCTION_NAME __func__
#else
# define CU_FUNCTION_NAME "(unknown function)"
#endif /* CU_SOURCE_LOCATION_SUPPORT */

/* unique variable name */
#if defined(CU_COUNTER_SUPPORT)
# define CU_UNIQUE_NAME(prefix) CU_MAGIC_CONCAT(CU_MAGIC_CONCAT(prefix, __COUNTER__), CU_LINE_NUMBER)
#else
# define CU_UNIQUE_NAME(prefix) CU_MAGIC_CONCAT(prefix, CU_LINE_NUMBER)
#endif

/* declaration of pure C code in the C++ header */
#ifndef CU_EXTERN_BEGIN_DECL
# if CU_COMPILER_IS_CXX
#  define CU_EXTERN_BEGIN_DECL extern "C" {
# else /* pure C */
#  define CU_EXTERN_BEGIN_DECL
# endif /* CU_COMPILER_IS_CXX */
#endif /* CU_EXTERN_BEGIN_DECL */
#ifndef CU_EXTERN_END_DECL
# if CU_COMPILER_IS_CXX
#  define CU_EXTERN_END_DECL }
# else /* pure C */
#  define CU_EXTERN_END_DECL
# endif /* CU_COMPILER_IS_CXX */
#endif /* CU_EXTERN_END_DECL */

/* external API call */
#ifndef CU_API_CALL
# if CU_OS_IS_WINDOWS
#  define CU_API_CALL __stdcall
# else /* !Windows */
#  define CU_API_CALL
# endif /* CU_OS_IS_WINDOWS */
#endif /* CU_API_CALL */

/* public declaration */
#ifndef CU_PUBLIC
# if CU_COMPILER_IS_MSVC
#  if defined(CU_EXPORT_IMPL)
#   define CU_PUBLIC __declspec(dllexport)
#  else /* !CU_EXPORT_IMPL */
#   define CU_PUBLIC __declspec(dllimport)
#  endif /* CU_EXPORT_IMPL */
# else /* !MSVC */
#  define CU_PUBLIC
# endif /* CU_COMPILER_IS_MSVC */
#endif /* CU_PUBLIC */

/* public function declaration */
#ifndef CU_PUBLIC_FUNC_DECL
# define CU_PUBLIC_FUNC_DECL(ret_type) CU_PUBLIC ret_type CU_API_CALL
#endif /* CU_PUBLIC_FUNC_DECL */


#endif /* CUDEF_HEADER_F93942F89D394708B216CCC1A5918577 */
