/*
    Cuprum library

    Core component - preprocessor library

    AOSP / Android
 */


#ifndef OPERATION_SYSTEM_ANDROID_HEADER_9B6E91426A234E76B6021E7BD6F125DB
#define OPERATION_SYSTEM_ANDROID_HEADER_9B6E91426A234E76B6021E7BD6F125DB


#ifndef CU_OS


/* detect Android as a special case for Linux kernel */
#if ( defined(linux)            \
    || defined(__linux)         \
    || defined(__linux__)       \
    || defined(__gnu_linux__) ) \
    && defined(__ANDROID__)


/* target operation system */
# define CU_OS CU_OS_ANDROID


#endif /* Android */


#endif /* CU_OS */


#endif /* OPERATION_SYSTEM_ANDROID_HEADER_9B6E91426A234E76B6021E7BD6F125DB */
