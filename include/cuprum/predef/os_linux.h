/*
    Cuprum library

    Core component - preprocessor library

    GNU Linux
 */


#ifndef OPERATION_SYSTEM_LINUX_HEADER_19F6DDD919CB48188C5858CD6553F755
#define OPERATION_SYSTEM_LINUX_HEADER_19F6DDD919CB48188C5858CD6553F755


#ifndef CU_OS


/* detect Linux but exclude Android */
#if ( defined(linux)            \
    || defined(__linux)         \
    || defined(__linux__)       \
    || defined(__gnu_linux__) ) \
    && !defined(__ANDROID__)


/* target operation system */
# define CU_OS CU_OS_LINUX


/* Linux features */
# if defined(__gnu_linux__) && !defined(_GNU_SOURCE)
#  define _GNU_SOURCE 1
# endif /* GNU Linux*/


#endif /* Linux */


#endif /* CU_OS */


#endif /* OPERATION_SYSTEM_LINUX_HEADER_19F6DDD919CB48188C5858CD6553F755 */
