/*

    Preprocessing component - Microsoft compiler

 */


#ifndef CU_COMPILER_MSVC_HEADER_CDF8A0B3819849F1834695DA007914DA
#define CU_COMPILER_MSVC_HEADER_CDF8A0B3819849F1834695DA007914DA


#ifndef CU_COMPILER


/* detect MSVC */
#if defined(_MSC_VER)


/* MSVC found */
#define CU_COMPILER CU_COMPILER_MSVC


/* MSVC version */
#if defined(_MSC_FULL_VER)
# if _MSC_VER >= 1400    /* MSVC2005+ */
#  define CU_COMPILER_VERSION                   \
     CU_COMPILER_VERSION_BUILDER(               \
         _MSC_FULL_VER / 10000000               \
         , (_MSC_FULL_VER % 10000000) / 100000  \
         , _MSC_FULL_VER % 100000               \
     )
# else
#  define CU_COMPILER_VERSION                   \
     CU_COMPILER_VERSION_BUILDER(               \
         _MSC_FULL_VER / 1000000                \
         , (_MSC_FULL_VER % 1000000) / 10000    \
         , _MSC_FULL_VER % 10000                \
     )
# endif
#else /* !_MSC_FULL_VER */
# define CU_COMPILER_VERSION                    \
    CU_COMPILER_VERSION_BUILDER(                \
        _MSC_VER / 100                          \
        , _MSC_VER % 100                        \
        , 0                                     \
    )
#endif /* _MSC_FULL_VER */


/* MSVC features */
#ifndef _CRT_SECURE_NO_WARNINGS
# define _CRT_SECURE_NO_WARNINGS 1
#endif /* _CRT_SECURE_NO_WARNINGS */

#ifndef _CRT_SECURE_NO_DEPRECATE
# define _CRT_SECURE_NO_DEPRECATE 1
#endif /* _CRT_SECURE_NO_DEPRECATE */

#if _MSC_VER >= 1200
# define CU_PRAGMA_ONCE_SUPPORT 1
#endif

/* MSVC has only partial support of latest C standards */
#if (CU_COMPILER_TYPE == CU_COMPILER_TYPE_C)
# if !defined(CU_COMPILER_STANDARD)
#  define CU_COMPILER_STANDARD CU_C_STD99
# endif
#endif /* C compiler */


#endif /* MSVC */


#endif /* CU_COMPILER */


#endif /* CU_COMPILER_MSVC_HEADER_CDF8A0B3819849F1834695DA007914DA */
