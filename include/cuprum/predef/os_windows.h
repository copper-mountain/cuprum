/*
    Cuprum library

    Core component - preprocessor library

    Microsoft Windows
 */


#ifndef OPERATION_SYSTEM_WINDOWS_HEADER_8C6FD7DFB29748279EF87C7882354E68
#define OPERATION_SYSTEM_WINDOWS_HEADER_8C6FD7DFB29748279EF87C7882354E68


#ifndef CU_OS


/* detect Microsoft Windows */
#if ( defined(WIN32)            \
    || defined(_WIN32)          \
    || defined(__WIN32__)       \
    || defined(WIN64)           \
    || defined(_WIN64)          \
    || defined(__WIN64__)       \
    || defined(__WINDOWS__) )   \


/* target operation system */
# define CU_OS CU_OS_WINDOWS


/* Windows features */
#ifndef NOMINMAX
# define NOMINMAX 1
#endif /* NOMINMAX */

#ifndef WIN32_LEAN_AND_MEAN
# define WIN32_LEAN_AND_MEAN 1
#endif /* WIN32_LEAN_AND_MEAN */


#endif /* Microsoft Windows */


#endif /* CU_OS */


#endif /* OPERATION_SYSTEM_WINDOWS_HEADER_8C6FD7DFB29748279EF87C7882354E68 */
