/*

    Preprocessing component - compiler type and standard

 */

#ifndef COMPILER_HEADER_80134D2B7C9A42A88E3E93287E43315D
#define COMPILER_HEADER_80134D2B7C9A42A88E3E93287E43315D


/* language */
#define CU_COMPILER_TYPE_C   ( 0xfe010000 )
#define CU_COMPILER_TYPE_CXX ( 0xfe020000 )


/* C standards, ANSI X3.159-1989 is ignored and treated as C90 */
#define CU_C_STD90 ( 0xfe010001 )  /* ISO/IEC 9899:1990    */
#define CU_C_STD94 ( 0xfe010002 )  /* ISO/IEC 9899-1:1990  */
#define CU_C_STD99 ( 0xfe010003 )  /* ISO/IEC 9899:1999    */
#define CU_C_STD11 ( 0xfe010004 )  /* ISO/IEC 9899:2011    */
#define CU_C_STD17 ( 0xfe010005 )  /* ISO/IEC 9899:2018    */
#define CU_C_STD23 ( 0xfe010006 )  /* ISO/IEC 9899:2024    */


/* C++ standards */
#define CU_CXX_STD98 ( 0xfe020001 )  /* ISO/IEC 14882:1998   */
#define CU_CXX_STD03 ( 0xfe020002 )  /* ISO/IEC 14882:2003   */
#define CU_CXX_STD11 ( 0xfe020003 )  /* ISO/IEC 14882:2011   */
#define CU_CXX_STD14 ( 0xfe020004 )  /* ISO/IEC 14882:2014   */
#define CU_CXX_STD17 ( 0xfe020005 )  /* ISO/IEC 14882:2017   */
#define CU_CXX_STD20 ( 0xfe020006 )  /* ISO/IEC 14882:2020   */
#define CU_CXX_STD23 ( 0xfe020007 )  /* ISO/IEC 14882:2023   */


/* detect compiler type and supported standards */
#if defined(__cplusplus)

# define CU_COMPILER_TYPE CU_COMPILER_TYPE_CXX

# if (__cplusplus >= 202302L)
#  define CU_COMPILER_STANDARD CU_CXX_STD23 /* C++23 */
# elif (__cplusplus >= 202002L)
#  define CU_COMPILER_STANDARD CU_CXX_STD20 /* C++20 */
# elif (__cplusplus >= 201703L)
#  define CU_COMPILER_STANDARD CU_CXX_STD17 /* C++17 */
# elif (__cplusplus >= 201402L)
#  define CU_COMPILER_STANDARD CU_CXX_STD14 /* C++14 */
# elif (__cplusplus >= 201103L)
#  define CU_COMPILER_STANDARD CU_CXX_STD11 /* C++11 */
# elif (__cplusplus >= 199711L)
    /* there is no way to determinate support C++03 or C++98 directly
        so let's be optimistic and treat this as C++03 */
#  define CU_COMPILER_STANDARD CU_CXX_STD03 /* C++03 */
# else
    /* unstandardized or unsupported compiler - fallback to the C++98 */
#  define CU_COMPILER_STANDARD CU_CXX_STD98 /* C++98 */
# endif /* cplusplus version */

#else /* __cplusplus */

# define CU_COMPILER_TYPE CU_COMPILER_TYPE_C

# if defined(__STDC__)
#  if defined(__STDC_VERSION__)
#   if (__STDC_VERSION__ >= 202311L)
#    define CU_COMPILER_STANDARD CU_C_STD23  /* C23 */
#   elif (__STDC_VERSION__ >= 201710L)
#    define CU_COMPILER_STANDARD CU_C_STD17  /* C17 */
#   elif (__STDC_VERSION__ >= 201112L)
#    define CU_COMPILER_STANDARD CU_C_STD11  /* C11 */
#   elif (__STDC_VERSION__ >= 199901L)
#    define CU_COMPILER_STANDARD CU_C_STD99  /* C99 */
#   elif (__STDC_VERSION__ >= 199409L)
#    define CU_COMPILER_STANDARD CU_C_STD94  /* C94 */
#   endif /* __STDC_VERSION__ value */
#  endif /* __STDC_VERSION__ */
# endif /* __STD__ */

/* fallback scenario to C90 */
# ifndef CU_COMPILER_STANDARD
#  define CU_COMPILER_STANDARD CU_C_STD90
# endif /* CU_COMPILER_STANDARD */

#endif /* __cplusplus */


/* compiler version helper */
#define CU_COMPILER_VERSION_BUILDER(v,r,p) \
    ( ( ( (v) * 10000000 ) + ( (r) * 100000 ) ) + (p) )


/* list of supported compilers */
#define CU_COMPILER_GCC     ( 0xfd000001 )  /* GNU GCC                          */
#define CU_COMPILER_MINGW   ( 0xfd000002 )  /* Minimalist GNU GCC for Windows   */
#define CU_COMPILER_CLANG   ( 0xfd000003 )  /* LLVM Clang                       */
#define CU_COMPILER_MSVC    ( 0xfd000004 )  /* Microsoft C and C++ compiler     */


/* detect compiler */
#include <cuprum/predef/cl_gcc.h>
#include <cuprum/predef/cl_mingw.h>
#include <cuprum/predef/cl_clang.h>
#include <cuprum/predef/cl_msvc.h>


/* help macros */
#define CU_COMPILER_IS_C        (CU_COMPILER_TYPE == CU_COMPILER_TYPE_C)
#define CU_COMPILER_IS_NOT_C    (CU_COMPILER_TYPE != CU_COMPILER_TYPE_C)

#define CU_COMPILER_IS_CXX      (CU_COMPILER_TYPE == CU_COMPILER_TYPE_CXX)
#define CU_COMPILER_IS_NOT_CXX  (CU_COMPILER_TYPE != CU_COMPILER_TYPE_CXX)

#define CU_COMPILER_STANDARD_IS_C90         ((CU_COMPILER_IS_C) && (CU_COMPILER_STANDARD == CU_C_STD90))
#define CU_COMPILER_STANDARD_IS_C90_UP      ((CU_COMPILER_IS_C) && (CU_COMPILER_STANDARD >  CU_C_STD90))
#define CU_COMPILER_STANDARD_IS_C94         ((CU_COMPILER_IS_C) && (CU_COMPILER_STANDARD == CU_C_STD94))
#define CU_COMPILER_STANDARD_IS_C94_UP      ((CU_COMPILER_IS_C) && (CU_COMPILER_STANDARD >  CU_C_STD94))
#define CU_COMPILER_STANDARD_IS_C99         ((CU_COMPILER_IS_C) && (CU_COMPILER_STANDARD == CU_C_STD99))
#define CU_COMPILER_STANDARD_IS_C99_UP      ((CU_COMPILER_IS_C) && (CU_COMPILER_STANDARD >  CU_C_STD99))
#define CU_COMPILER_STANDARD_IS_C11         ((CU_COMPILER_IS_C) && (CU_COMPILER_STANDARD == CU_C_STD11))
#define CU_COMPILER_STANDARD_IS_C11_UP      ((CU_COMPILER_IS_C) && (CU_COMPILER_STANDARD >  CU_C_STD11))
#define CU_COMPILER_STANDARD_IS_C17         ((CU_COMPILER_IS_C) && (CU_COMPILER_STANDARD == CU_C_STD17))
#define CU_COMPILER_STANDARD_IS_C17_UP      ((CU_COMPILER_IS_C) && (CU_COMPILER_STANDARD >  CU_C_STD17))
#define CU_COMPILER_STANDARD_IS_C23         ((CU_COMPILER_IS_C) && (CU_COMPILER_STANDARD >= CU_C_STD23))

#define CU_COMPILER_STANDARD_IS_CXX98       ((CU_COMPILER_IS_CXX) && (CU_COMPILER_STANDARD == CU_CXX_STD98))
#define CU_COMPILER_STANDARD_IS_CXX98_UP    ((CU_COMPILER_IS_CXX) && (CU_COMPILER_STANDARD >  CU_CXX_STD98))
#define CU_COMPILER_STANDARD_IS_CXX03       ((CU_COMPILER_IS_CXX) && (CU_COMPILER_STANDARD == CU_CXX_STD03))
#define CU_COMPILER_STANDARD_IS_CXX03_UP    ((CU_COMPILER_IS_CXX) && (CU_COMPILER_STANDARD >  CU_CXX_STD03))
#define CU_COMPILER_STANDARD_IS_CXX11       ((CU_COMPILER_IS_CXX) && (CU_COMPILER_STANDARD == CU_CXX_STD11))
#define CU_COMPILER_STANDARD_IS_CXX11_UP    ((CU_COMPILER_IS_CXX) && (CU_COMPILER_STANDARD >  CU_CXX_STD11))
#define CU_COMPILER_STANDARD_IS_CXX14       ((CU_COMPILER_IS_CXX) && (CU_COMPILER_STANDARD == CU_CXX_STD14))
#define CU_COMPILER_STANDARD_IS_CXX14_UP    ((CU_COMPILER_IS_CXX) && (CU_COMPILER_STANDARD >  CU_CXX_STD14))
#define CU_COMPILER_STANDARD_IS_CXX17       ((CU_COMPILER_IS_CXX) && (CU_COMPILER_STANDARD == CU_CXX_STD17))
#define CU_COMPILER_STANDARD_IS_CXX17_UP    ((CU_COMPILER_IS_CXX) && (CU_COMPILER_STANDARD >  CU_CXX_STD17))
#define CU_COMPILER_STANDARD_IS_CXX20       ((CU_COMPILER_IS_CXX) && (CU_COMPILER_STANDARD == CU_CXX_STD20))
#define CU_COMPILER_STANDARD_IS_CXX20_UP    ((CU_COMPILER_IS_CXX) && (CU_COMPILER_STANDARD >  CU_CXX_STD20))
#define CU_COMPILER_STANDARD_IS_CXX23       ((CU_COMPILER_IS_CXX) && (CU_COMPILER_STANDARD >= CU_CXX_STD23))


#define CU_COMPILER_IS_GCC      (CU_COMPILER == CU_COMPILER_GCC)
#define CU_COMPILER_IS_CLANG    (CU_COMPILER == CU_COMPILER_CLANG)
#define CU_COMPILER_IS_MINGW    (CU_COMPILER == CU_COMPILER_MINGW)
#define CU_COMPILER_IS_MSVC     (CU_COMPILER == CU_COMPILER_MSVC)


/* C++20 features */
#if CU_COMPILER_STANDARD_IS_CXX17_UP
# include <version>
#endif /* C++20 */


#endif /* COMPILER_HEADER_80134D2B7C9A42A88E3E93287E43315D */
