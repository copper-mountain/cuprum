/*

    Preprocessing component - Minimalistic GNU for Windows compiler

 */


#ifndef CU_COMPILER_MINGW_HEADER_A07BDCE243EC415893BB2A7F38C28392
#define CU_COMPILER_MINGW_HEADER_A07BDCE243EC415893BB2A7F38C28392


#ifndef CU_COMPILER


/* detect MinGW */
#if defined(__GNUC__) && defined(__MINGW32__)


/* MinGW found */
#define CU_COMPILER CU_COMPILER_MINGW


/* take GCC version,
 * because MinGW definitions show MinGW package version but not a compiler */
#if defined(__GNUC_PATCHLEVEL__)
# define CU_COMPILER_VERSION        \
    CU_COMPILER_VERSION_BUILDER(    \
        __GNUC__                    \
        , __GNUC_MINOR__            \
        , __GNUC_PATCHLEVEL__       \
    )
#else /* !__GNUC_PATCHLEVEL__ */
# define CU_COMPILER_VERSION        \
    CU_COMPILER_VERSION_BUILDER(    \
        __GNUC__                    \
        , __GNUC_MINOR__            \
        , 0                         \
    )
#endif /* __GNUC_PATCHLEVEL__ */


/* MinGW features */
#if (CU_COMPILER_VERSION >= CU_COMPILER_VERSION_BUILDER(3,4,0))
# define CU_PRAGMA_ONCE_SUPPORT 1
#endif /* MinGW v3.4.0 */


#endif /* MinGW */


#endif /* CU_COMPILER */


#endif /* CU_COMPILER_MINGW_HEADER_A07BDCE243EC415893BB2A7F38C28392 */
