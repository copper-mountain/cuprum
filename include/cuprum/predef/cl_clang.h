/*

    Preprocessing component - Clang compiler

 */


#ifndef CU_COMPILER_CLANG_HEADER_392925D371C043348DCE63E2771D6316
#define CU_COMPILER_CLANG_HEADER_392925D371C043348DCE63E2771D6316


#ifndef CU_COMPILER


/* detect Clang but not GCC */
#if defined(__GNUC__) && defined(__clang__)


/* Clang found */
#define CU_COMPILER CU_COMPILER_CLANG


/* Clang version */
#if defined(__clang_patchlevel__)
# define CU_COMPILER_VERSION        \
    CU_COMPILER_VERSION_BUILDER(    \
        __clang_major__             \
        , __clang_minor__           \
        , __clang_patchlevel__      \
    )
#else /* !__GNUC_PATCHLEVEL__ */
# define CU_COMPILER_VERSION        \
    CU_COMPILER_VERSION_BUILDER(    \
        __clang_major__             \
        , __clang_minor__           \
        , 0                         \
    )
#endif /* __GNUC_PATCHLEVEL__ */


/* Clang features */
#define CU_PRAGMA_ONCE_SUPPORT 1


#endif /* Clang */


#endif /* CU_COMPILER */


#endif /* CU_COMPILER_CLANG_HEADER_392925D371C043348DCE63E2771D6316 */
