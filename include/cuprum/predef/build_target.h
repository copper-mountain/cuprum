/*

    Preprocessing component - build target

 */

#ifndef BUILD_TARGET_HEADER_39709637B46F49658890B85C2293DF85
#define BUILD_TARGET_HEADER_39709637B46F49658890B85C2293DF85


/* Cuprum library supports only two classical build types:
    - Debug
    - Release

   Cmake variant RelWithDebugInfo is treating as Release */

/* supported build types */
#define CU_BUILD_TYPE_DEBUG   ( 0xff000001 )
#define CU_BUILD_TYPE_RELEASE ( 0xff000002 )

/* undefined or default build type is treating as release */
#if ( ( defined(DEBUG) || defined(_DEBUG) ) \
        && ( !defined(NDEBUG) ) )
# define CU_BUILD_TYPE CU_BUILD_TYPE_DEBUG
#else
# define CU_BUILD_TYPE CU_BUILD_TYPE_RELEASE
#endif

/* helping macros */
#define CU_BUILD_TYPE_IS_DEBUG       (CU_BUILD_TYPE == CU_BUILD_TYPE_DEBUG)
#define CU_BUILD_TYPE_IS_NOT_DEBUG   (CU_BUILD_TYPE != CU_BUILD_TYPE_DEBUG)
#define CU_BUILD_TYPE_IS_RELEASE     (CU_BUILD_TYPE == CU_BUILD_TYPE_RELEASE)
#define CU_BUILD_TYPE_IS_NOT_RELEASE (CU_BUILD_TYPE != CU_BUILD_TYPE_RELEASE)


#endif /* BUILD_TARGET_HEADER_39709637B46F49658890B85C2293DF85 */
