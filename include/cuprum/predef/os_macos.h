/*
    Cuprum library

    Core component - preprocessor library

    Apple MacOS
 */


#ifndef OPERATION_SYSTEM_MACOS_HEADER_95B7E047923E43B69C3621413228CEAC
#define OPERATION_SYSTEM_MACOS_HEADER_95B7E047923E43B69C3621413228CEAC


#ifndef CU_OS


/* detect MacOS */
#if defined(__APPLE__) && defined(__MACH__)


/* include Apple target header */
#include <TargetConditionals.h>


#if defined(TARGET_OS_MAC) && ( defined(TARGET_OS_OSX) && (TARGET_OS_OSX == 1))


/* target operation system */
# define CU_OS CU_OS_MACOS


#endif /* MacOS */

#endif /* Apple */


#endif /* CU_OS */


#endif /* OPERATION_SYSTEM_MACOS_HEADER_95B7E047923E43B69C3621413228CEAC */
