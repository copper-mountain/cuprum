/*

    Preprocessing component - GCC compiler

 */


#ifndef CU_COMPILER_GCC_HEADER_0662F4CAC1E04939A1CED10C91EBD3B0
#define CU_COMPILER_GCC_HEADER_0662F4CAC1E04939A1CED10C91EBD3B0


#ifndef CU_COMPILER


/* detect GCC but not Clang or MinGW */
#if defined(__GNUC__) && !defined(__clang__) && !defined(__MINGW32__)


/* GNU found */
#define CU_COMPILER CU_COMPILER_GCC


/* GCC version */
#if defined(__GNUC_PATCHLEVEL__)
# define CU_COMPILER_VERSION        \
    CU_COMPILER_VERSION_BUILDER(    \
        __GNUC__                    \
        , __GNUC_MINOR__            \
        , __GNUC_PATCHLEVEL__       \
    )
#else /* !__GNUC_PATCHLEVEL__ */
# define CU_COMPILER_VERSION        \
    CU_COMPILER_VERSION_BUILDER(    \
        __GNUC__                    \
        , __GNUC_MINOR__            \
        , 0                         \
    )
#endif /* __GNUC_PATCHLEVEL__ */


/* GCC features */
#if (CU_COMPILER_VERSION >= CU_COMPILER_VERSION_BUILDER(3,4,0))
# define CU_PRAGMA_ONCE_SUPPORT 1
#endif /* GCC v3.4.0 */


#endif /* GCC */


#endif /* CU_COMPILER */


#endif /* CU_COMPILER_GCC_HEADER_0662F4CAC1E04939A1CED10C91EBD3B0 */
