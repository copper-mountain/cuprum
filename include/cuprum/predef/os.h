/*

    Preprocessing component - target operation system

 */


#ifndef OPERATION_SYSTEM_HEADER_19F6DDD919CB48188C5858CD6553F755
#define OPERATION_SYSTEM_HEADER_19F6DDD919CB48188C5858CD6553F755


/* list of supported operation systems */
#define CU_OS_LINUX     ( 0xfc000001 )
#define CU_OS_ANDROID   ( 0xfc000002 )
#define CU_OS_WINDOWS   ( 0xfc000003 )
#define CU_OS_MACOS     ( 0xfc000004 )
#define CU_OS_IOS       ( 0xfc000005 )


/* detect target operation system */
#include <cuprum/predef/os_linux.h>
#include <cuprum/predef/os_android.h>
#include <cuprum/predef/os_windows.h>
#include <cuprum/predef/os_macos.h>
#include <cuprum/predef/os_ios.h>


/* helpers */
#define CU_OS_IS_LINUX      (CU_OS == CU_OS_LINUX)
#define CU_OS_IS_ANDROID    (CU_OS == CU_OS_ANDROID)
#define CU_OS_IS_WINDOWS    (CU_OS == CU_OS_WINDOWS)
#define CU_OS_IS_MACOS      (CU_OS == CU_OS_MACOS)
#define CU_OS_IS_IOS        (CU_OS == CU_OS_IOS)

#define CU_OS_IS_POSIX      (CU_OS_IS_LINUX \
                            || CU_OS_IS_ANDROID \
                            || CU_OS_IS_MACOS \
                            || CU_OS_IS_IOS)
#define CU_OS_IS_NOT_POSIX  (CU_OS_IS_WINDOWS)


#endif /* OPERATION_SYSTEM_HEADER_19F6DDD919CB48188C5858CD6553F755 */
