/*
    Cuprum library

    Core component - preprocessor library

    Apple MacOS
 */


#ifndef OPERATION_SYSTEM_IOS_HEADER_7499B47BA8C542BBBE13FFAE4DDA8E70
#define OPERATION_SYSTEM_IOS_HEADER_7499B47BA8C542BBBE13FFAE4DDA8E70


#ifndef CU_OS


/* detect MacOS */
#if defined(__APPLE__) && defined(__MACH__)


/* include Apple target header */
#include <TargetConditionals.h>


#if defined(TARGET_OS_MAC) && ( defined(TARGET_OS_IOS) && (TARGET_OS_IOS == 1))


/* target operation system */
# define CU_OS CU_OS_IOS


#endif /* iOS */

#endif /* Apple */


#endif /* CU_OS */


#endif /* OPERATION_SYSTEM_IOS_HEADER_7499B47BA8C542BBBE13FFAE4DDA8E70 */
