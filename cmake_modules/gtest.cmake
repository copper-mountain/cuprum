include( FetchContent )

function( FETCHGOOGLETESTFRAMEWORK )
    message( "Fetching Google Test Framework from GitHub repository")
    message( CHECK_START "Fetching..." )
    list( APPEND CMAKE_MESSAGE_INDENT "  " )

        # fetch given version of Google Tests
        FetchContent_Declare(
            googletest
            GIT_REPOSITORY https://github.com/google/googletest.git
            GIT_TAG        v1.15.2
        )

        # setup Google Tests specific options
        set( gmock_build_tests OFF )
        set( gtest_build_tests OFF )
        set( gtest_build_samples OFF )
        set( INSTALL_GTEST OFF )

        if( MSVC )
            set( gtest_force_shared_crt ON CACHE BOOL "" FORCE )          
        endif()

        # force CMake to set CMP0077 to resolve the GTest warning
        set( CMAKE_POLICY_DEFAULT_CMP0077 NEW )

        FetchContent_MakeAvailable( googletest )

        message( CHECK_PASS "done" )

        message( CHECK_START "Proceed Google Test Framework settings..." )

        set( GTest_FOUND true PARENT_SCOPE )
        set( GTEST_ROOT ${googletest_SOURCE_DIR} )
        set( GTEST_ROOT ${GTEST_ROOT} PARENT_SCOPE )
        set( GTEST_INCLUDE_DIR ${GTEST_ROOT}/googletest/include PARENT_SCOPE )
        set( GMOCK_INCLUDE_DIR ${GTEST_ROOT}/googlemock/include PARENT_SCOPE )
        set( GTEST_LIBRARIES gtest )
        set( GTEST_LIBRARIES ${GTEST_LIBRARIES} PARENT_SCOPE )
        set( GTEST_MAIN_LIBRARIES gtest_main )
        set( GTEST_MAIN_LIBRARIES ${GTEST_MAIN_LIBRARIES} PARENT_SCOPE )
        set( GTEST_BOTH_LIBRARIES ${GTEST_LIBRARIES} ${GTEST_MAIN_LIBRARIES} PARENT_SCOPE )
        set( GMOCK_LIBRARIES gmock )
        set( GMOCK_LIBRARIES ${GMOCK_LIBRARIES} PARENT_SCOPE )

        message( CHECK_PASS "done" )

    list( POP_BACK CMAKE_MESSAGE_INDENT )

endfunction() # fetch_GoogleTestFramework
