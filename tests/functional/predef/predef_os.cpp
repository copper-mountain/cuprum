/* Functional test unit of predefined library
 *
 * operation system detection */


#include <cuprum/cudef.h>
#include <gtest/gtest.h>


#include "predef_os.hpp"


/* mock function to translate detected operation system to string */
const char* decode_target_os() {
    switch (CU_OS)
    {
    case CU_OS_LINUX:
        return "Linux";
    case CU_OS_ANDROID:
        return "Android";
    case CU_OS_WINDOWS:
        return "Windows";
    case CU_OS_MACOS:
        return "Darwin";
    case CU_OS_IOS:
        return "iOS";
    default:
        return "unknown";
    }
}


TEST(CuprumFunctionalPredefOS, TargetOSIsDetected) {
    EXPECT_TRUE( CU_OS )
        << "Target operation system MUST be detected";
}

TEST(CuprumFunctionalPredefOS, TargetOSDetectionIsCorrect) {
    EXPECT_STRCASEEQ( decode_target_os(), CMAKE_SYSTEM_NAME )
        << "Target operation system MUST be properly detected";
}

