/* Functional test unit of predefined library
 *
 * compiler detection */


#include <cuprum/cudef.h>
#include <gtest/gtest.h>
#include <gmock/gmock.h>


#include "predef_compiler.hpp"


using ::testing::HasSubstr;


/* mock function to translate detected compiler to string */
const char* decode_compiler() {
    /* CMake treat MinGW as GNU (GCC) */
    switch (CU_COMPILER)
    {
    case CU_COMPILER_GCC:
    case CU_COMPILER_MINGW:
        return "GNU";
    case CU_COMPILER_CLANG:
        return "Clang";
    case CU_COMPILER_MSVC:
        return "MSVC";
    default:
        return "unknown";
    }
}


/* mock function to translate detected compiler version to string */
std::string decode_compiler_version() {
    int version  = ( CU_COMPILER_VERSION / 10000000 );
    int revision = ( CU_COMPILER_VERSION % 10000000 ) / 100000;
    int patch    =   CU_COMPILER_VERSION % 100000;

    std::string result =
        std::to_string( version )
        + "."
        + std::to_string( revision )
        + "."
        + std::to_string( patch );

    return result;
}


/* mock function to translate detected C++ standard to int value */
int decode_compiler_standard() {
    switch (CU_COMPILER_STANDARD)
    {
    case CU_CXX_STD23:
        return 23;
    case CU_CXX_STD20:
        return 20;
    case CU_CXX_STD17:
        return 17;
    case CU_CXX_STD14:
        return 14;
    case CU_CXX_STD11:
        return 11;
    case CU_CXX_STD03:
        return 03;
    case CU_CXX_STD98:
        return 98;
    default:
        return -1;
    }
}


TEST(CuprumFunctionalPredefCompiler, CompilerIsDetected) {
    EXPECT_TRUE( CU_COMPILER )
        << "Compiler MUST be detected";
}


TEST(CuprumFunctionalPredefCompiler, CompilerDetectionIsCorrect) {
    EXPECT_THAT( CMAKE_CXX_COMPILER_ID, HasSubstr( decode_compiler() ) )
        << "Compiler MUST be properly detected";
}


TEST(CuprumFunctionalPredefCompiler, CompilerTypeIsCxx) {
    EXPECT_EQ( CU_COMPILER_TYPE, CU_COMPILER_TYPE_CXX )
        << "Sanity check, because functional tests have written with C++, MUST never fail";
}


TEST(CuprumFunctionalPredefCompiler, CompilerVersion) {
    EXPECT_THAT( CMAKE_CXX_COMPILER_VERSION, HasSubstr( decode_compiler_version() ) )
        << "Compiler version MUST be properly detected";
}


TEST(CuprumFunctionalPredefCompiler, CompilerStandartSupport) {
    std::string current_standard_str = CMAKE_CXX_STANDARD;
    if( current_standard_str.empty() )
        current_standard_str = CMAKE_CXX_STANDARD_DEFAULT;
    int current_standard = std::stoi( current_standard_str );

    EXPECT_EQ( decode_compiler_standard(), current_standard )
        << "Compiler standard MUST be properly detected";
}
