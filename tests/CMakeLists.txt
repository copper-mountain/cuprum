
include( gtest )

# fetch GTest
FetchGoogleTestFramework()


# declare a target for all tests
set( cuprum_tests_project cu_tests )
set( targets ${cuprum_tests_project} )
add_custom_target( ${cuprum_tests_project} )


# process tests
add_subdirectory( functional )
