FROM alpine:3.21

# setup timezone
RUN apk add --quiet --no-cache --update tzdata && \
    ln -fs  /usr/share/zoneinfo/Europe/Amsterdam /etc/loclatime && \
    echo "Europe/Amsterdam" > /etc/timezone

# update and install all packages
RUN apk update --quiet --update-cache && \
    apk update --quiet && \
    apk add --quiet \
        git \
        openssh-client \
        build-base \
        clang \
        cmake \
        extra-cmake-modules \
        python3 \
        py3-pip \
        wget \
        unzip \
        mingw-w64-binutils \
        mingw-w64-crt \
        mingw-w64-winpthreads \
        mingw-w64-gcc \
        wine

# set up non root user
RUN adduser -D user && \
    mkdir -p /opt/python_env && \
    chmod 777 /opt/python_env
USER user

# add requirements for the python environment
ADD python/requirements.txt /opt/python_env/requirements.txt

# install Python packages for the user
RUN python3 -m venv /opt/python_env && \
    . /opt/python_env/bin/activate && \
        pip3 -q install --upgrade pip && \
        pip3 -q install --use-pep517 -r /opt/python_env/requirements.txt && \
    deactivate

# provide path for wine libraries
ENV WINEPATH=/usr/x86_64-w64-mingw32/bin/
