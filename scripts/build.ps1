param([switch]$help, [switch]$clean, $type, $target)

$build_dir_path = ($PWD).Path + "\build_dir"

function Invoke-Cmake {
    param([string]$cmake_args)

    $cmake_res = Start-Process -FilePath "cmake.exe" -ArgumentList $cmake_args -NoNewWindow -Wait -PassThru
    if( $cmake_res.ExitCode -ne 0 ) {
        exit 1
    }
}

# print usage
if( $help) {
    echo "usage:"
    echo "    .\scripts\build.ps1 [-help] [-clean] [-type <build type>] -target <CMake target>"
    echo ""
    echo "    -help     print usage information"
    echo "    -clean    clean build, all artefacts from the previous build will be removed"
    echo "    -type     build type: Debug or Release"
    echo "    -target   CMake target name to build, mandatory parameter"
    exit 0
}

if( [string]::IsNullOrEmpty($target) ) {
    echo "error: target to build is not defined"
    exit 1
}

# delete output folder
if( $clean ) {
    if( Test-Path $build_dir_path ) {
        Remove-Item $build_dir_path -Recurse -Force
    }
}

if( [string]::IsNullOrEmpty($type) ) {
    $type = "Release"
}

echo "prepearing the build folder..."

$prepare_args = "-A x64 -S . -B $build_dir_path"

Invoke-Cmake $prepare_args

echo "build the library..."

$build_args = "--build $build_dir_path --config $type"

if( -not ( [string]::IsNullOrEmpty($target) ) ) {
    $build_args =  "$build_args --target $target"
}

Invoke-Cmake $build_args
