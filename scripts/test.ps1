param ([switch]$help, $type, [switch]$parallel, [switch]$random)

$python_env_path = ($PWD).Path + "\python_env"
$test_dir_path = ($PWD).Path + "\build_dir\tests"
$pytest_results = $test_dir_path + "\test_results.xml"

function Enable-PythonEnv {
    if( Test-Path $python_env_path ) {
        .\python_env\Scripts\Activate.ps1
    }
}

function Disable-PythonEnv {
    if( Test-Path $python_env_path ) {
        deactivate
    }
}

function Invoke-PyTest {
    param([string]$pytest_args)

    $python_args = "-m pytest $pytest_args"

    $cmake_res = Start-Process -FilePath "python.exe" -ArgumentList $python_args -NoNewWindow -Wait -PassThru
    if( $cmake_res.ExitCode -ne 0 ) {
        exit 1
    }
}
# print usage
if( $help) {
    echo "usage:"
    echo "    .\scripts\build.ps1 [-help] [-type <tests type>] [-parallel] [-random]"
    echo ""
    echo "    -help     print usage information"
    echo "    -type     type of tests which is subfolder in the test folder, ex.: unit or functional"
    echo "    -parallel run all tests in parallel"
    echo "    -random   run all tests in random order"
    exit 0
}

Enable-PythonEnv

if( -not ( [string]::IsNullOrEmpty($type) ) ) {
    $test_dir_path = "$test_dir_path" + "\" + $type

    if( -not ( Test-Path $test_dir_path ) ) {
        echo "error: there is no tests type like $type"
        exit 1
    }
}

if( -not ( Test-Path $test_dir_path ) ) {
    echo "error: test folder $test_dir_path doesn't exist"
    exit 1
}

$pytest_args = "--junit-xml=$pytest_results"

# parallel execution
if( $parallel ) {
    $pytest_args = "$pytest_args -n auto"
}

# random order
if( $random ) {
    $pytest_args = "$pytest_args --random-order"
}

$pytest_args = "$pytest_args $test_dir_path"

Invoke-PyTest $pytest_args

Disable-PythonEnv
